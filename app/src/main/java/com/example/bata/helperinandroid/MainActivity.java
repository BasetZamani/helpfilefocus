package com.example.bata.helperinandroid;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import uk.co.samuelwall.materialtaptargetprompt.MaterialTapTargetPrompt;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

      final   MaterialTapTargetPrompt.Builder b3=  new MaterialTapTargetPrompt.Builder(MainActivity.this)
                .setTarget(R.id.button)
                .setPrimaryText("this is BUton 4")
                .setSecondaryText("this is description")
                .setPromptStateChangeListener(new MaterialTapTargetPrompt.PromptStateChangeListener()
                {
                    @Override
                    public void onPromptStateChanged(MaterialTapTargetPrompt prompt, int state)
                    {
                        if (state == MaterialTapTargetPrompt.STATE_DISMISSED||state == MaterialTapTargetPrompt.STATE_FOCAL_PRESSED)
                        {


                        }
                    }
                })
                ;


      final   MaterialTapTargetPrompt.Builder b2=  new MaterialTapTargetPrompt.Builder(MainActivity.this)
                .setTarget(R.id.textView)
                .setPrimaryText("this is helo world")
                .setSecondaryText("this is description")
                .setPromptStateChangeListener(new MaterialTapTargetPrompt.PromptStateChangeListener()
                {
                    @Override
                    public void onPromptStateChanged(MaterialTapTargetPrompt prompt, int state)
                    {
                        if (state == MaterialTapTargetPrompt.STATE_DISMISSED||state == MaterialTapTargetPrompt.STATE_FOCAL_PRESSED)
                        {
                            b3.show();

                        }
                    }
                })
                ;


       final MaterialTapTargetPrompt.Builder b1= new MaterialTapTargetPrompt.Builder(MainActivity.this)
                .setTarget(R.id.button2)
                .setPrimaryText("this is Button 2")
                .setSecondaryText("this is description")
                .setPromptStateChangeListener(new MaterialTapTargetPrompt.PromptStateChangeListener()
                {
                    @Override
                    public void onPromptStateChanged(MaterialTapTargetPrompt prompt, int state)
                    {
                        if (state == MaterialTapTargetPrompt.STATE_DISMISSED||state == MaterialTapTargetPrompt.STATE_FOCAL_PRESSED)
                        {
                            b2.show();
                        }
                    }
                })
                ;
        b1.show();

    }
}
